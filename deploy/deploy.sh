echo 1
set -e
echo 2
eval $(ssh-agent -s)
echo 3
echo "$PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
echo 4
mkdir -p ~/.ssh
echo 5
touch ~/.ssh/config
echo 6
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" >> ~/.ssh/config
echo 7
DEPLOY_SERVERS=$DEPLOY_SERVERS
echo 8
ssh ubuntu@${DEPLOY_SERVERS} 'bash' < ./deploy/updateAndRestart.sh
echo 9

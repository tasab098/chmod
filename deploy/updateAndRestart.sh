
set -e
rm -rf /home/ubuntu/test/
git clone https://gitlab.com/deniskoliban/test.git
ls -a
curl -sL https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.0/install.sh -o install_nvm.sh
bash install_nvm.sh
source /home/ubuntu/.nvm/nvm.sh
command -v nvm
nvm install --lts
npm install pm2 -g
pm2 kill
npm remove pm2 -g
npm install pm2 -g
cd /home/ubuntu/test/
npm install -g nx
npm install
nx build express-app
cd /home/ubuntu/test/dist/apps/express-app
pm2 start main.js
pm2 status

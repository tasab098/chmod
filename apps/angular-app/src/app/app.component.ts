import {Component, OnInit} from '@angular/core';
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'coding-hell-test-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit{
  title = 'angular-app';
  response: any = null;
  constructor(private http: HttpClient) {
  }

  ngOnInit(): void {
  }

  getRequest(): void {
    console.log("hello");
    this.http.get('http://ec2-52-24-148-80.us-west-2.compute.amazonaws.com:3333/api')
      .subscribe((response) => {
        console.log(response);
      })
  }

}
